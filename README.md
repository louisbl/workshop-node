Projet Node Js  -  Chloé DAVID BDDI, Gobelins Annecy

Mes choix :

J'ai décider de traiter les données Twitter avec des filtres.
Les twits qui sont recupérés en direct peuvent etre filtrés par mots clés, par langue et par localisation (ce dernier et sensible à la casse).
Chaque txit apparait de la couleur du compte du l'utilisateur à l'origine du twit.

Mes difficultés :

Arriver à filtrer les twits d'un flux en direct. J'ai d'abbord essayé de filter les twits à la racine : avant qu'ils soient envoyé au client, mais je rencontrais de problèmes car ça entravait le bon fonctionnement de mon stream. j'ai finis par faire le trie directement dans le client.

Installer le projet
npm install


Lancer le projet :
node index.js